package ca.claurendeau.service;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ca.claurendeau.domaine.Personne;
import ca.claurendeau.repositories.PersonneRepository;

@Service
public class PersonneService {
    
    @Autowired
    private PersonneRepository personneRepository;
    
    @Transactional
    public void createInitialPersonnes() {
        personneRepository.save(new Personne("Pascale Bouchard", "pbou@gmail.com"));
        personneRepository.save(new Personne("Jules Choinard", "jules@gmail.com"));
        personneRepository.save(new Personne("Francois Lacoursiere", "lacouf@gmail.com"));
    }
}
